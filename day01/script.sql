-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2023 at 05:17 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `QLSV`
--

-- --------------------------------------------------------

--
-- Table structure for table `DMKHOA`
--

CREATE TABLE `DMKHOA` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `DMKHOA`
--

INSERT INTO `DMKHOA` (`MaKH`, `TenKhoa`) VALUES
('1', 'Công nghệ thông tin'),
('2', 'Toán'),
('3', 'Công nghệ thông tin'),
('4', 'Văn');

-- --------------------------------------------------------

--
-- Table structure for table `SINHVIEN`
--

CREATE TABLE `SINHVIEN` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) NOT NULL,
  `TenSV` varchar(15) NOT NULL,
  `GioiTinh` char(1) NOT NULL,
  `NgaySinh` datetime NOT NULL,
  `NoiSinh` varchar(50) NOT NULL,
  `DiaChi` varchar(50) NOT NULL,
  `MaKH` varchar(6) NOT NULL,
  `HocBong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `SINHVIEN`
--

INSERT INTO `SINHVIEN` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES
('1', 'Tran', 'Duy', 'M', '2023-09-14 17:06:55', 'Nam Dinh', 'Nam Dinh', '123', 1200),
('3', 'Tran', 'Bach', 'N', '2023-09-14 17:06:55', 'Ha noi', 'Ha noi', '234', 1000);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
