<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="styles.css">
    <title>Đăng Ký Tân Sinh Viên</title>
</head>

<body>
    <form action="" class="bd-blue">
        <table class="w-100">
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="name">Họ và tên</label></td>
                <td class="bd-blue">
                    <input type="text" id="name" name="name" required class=" w-100 p-10-20 h-100">
                </td>
            </tr>
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="gender">Giới tính</label></td>
                <td>
                    <div class="d-flex">
                        <div class='genders'>
                            <input id='men' type='radio' name='genders' value='Nam'>
                            <label for='men'>Nam</label>
                        </div>
                        <div class='genders'>
                            <input id='women' type='radio' name='genders' value='Nữ'>
                            <label for='women'>Nữ</label>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="department">Phân khoa</label></td>
                <td>
                    <select id="department" name="department" class="bd-blue py-10">
                        <option value="">--Chọn phân khoa--</option>
                        <option value="MAT">Khoa học máy tính</option>
                        <option value="KDL">Khoa học vật liệu</option>
                    </select>
                </td>
            </tr>
        </table>
        <div class="text-center ">
            <button type="submit" class="btn bd-blue bg-green text-white mt-20">Đăng ký</button>
        </div>
    </form>
</body>

</html>
