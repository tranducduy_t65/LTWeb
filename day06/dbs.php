<?php
$servername = "localhost";
$username = "root";
$password = "";
$port = 8080;
$db_name = "phpWeb";
try {
  $conn = new PDO("mysql:host=$servername;port=$port;dbname=$db_name", $username, $password);

  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo "Connected successfully";
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
  echo "123456";
  die();
}
?>