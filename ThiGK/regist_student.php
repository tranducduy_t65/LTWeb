<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
    <div class="container">
        <form id="registrationForm" class="bd-blue" method="POST" enctype="multipart/form-data">
        <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20" for="name">Họ và
                    tên</div>

                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['name'];
                    ?>
                </div>
        </div>

            <div class="form-group">
                <div class="bg-green text-white  bd-blue p-10-20 w-30 text-center required-label me-20 " for="gender">
                    Giới tính</div>
                <div id="gender" name="gender" class="fl-1 p-10-20">
                        <?php
                        echo $_POST['gender'];
                        ?>
                </div>
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20 w-170"
                    for="birthdate">Ngày sinh</div>
                <div id="birthdate" name="birthdate" class="fl-1 p-10-20">
                    <?php
                    echo $_POST['birthdate'];
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20 w-170"
                    for="address">Địa chỉ</div>
                <div id="address" name="address" class="fl-1 p-10-20">
                    <?php
                    echo $_POST['address'];
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20 w-170"
                    for="infor">Thông tin khác</div>
                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['infor'];
                    ?>
                </div>
            </div>

            <div class="button-container" id="registerButton">
                <button type="submit">Xác nhận</button>
            </div>
        </form>
    </div>
</body>

</html>
