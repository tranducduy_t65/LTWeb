<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#submitButton").click(function() {
                if (errorMessages.length > 0) {
                    var errorMessageHtml = "";
                    for (var i = 0; i < errorMessages.length; i++) {
                        errorMessageHtml += errorMessages[i] + "<br>";
                    }
                    $("#errorMessages").html(errorMessageHtml);
                } else {
                    // If no errors, proceed with form submission or handling here
                    $("#errorMessages").html(""); // Clear old error messages
                }
            });
        });

        function updateDistricts() {
            var selectedCity = document.getElementById("city").value;
            var districtSelect = document.getElementById("district");
            districtSelect.innerHTML = ""; // Xóa các quận hiện tại

            if (selectedCity === "Hà Nội") {
                var hanoiDistricts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
                for (var i = 0; i < hanoiDistricts.length; i++) {
                    var option = document.createElement("option");
                    option.text = hanoiDistricts[i];
                    districtSelect.add(option);
                }
            } else if (selectedCity === "TP.HCM") {
                var hcmDistricts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
                for (var i = 0; i < hcmDistricts.length; i++) {
                    var option = document.createElement("option");
                    option.text = hcmDistricts[i];
                    districtSelect.add(option);
                }
            }
        }
    </script>
</head>

<body>
    <div class="container">
        <form id="registrationForm" class="bd-blue" method="POST" action="regist_student.php" enctype="multipart/form-data">
            <div class="text-center bold-text mb-20">Form đăng ký sinh viên</div>
            <div id="errorMessages" class="error">
            </div>

            <div class="form-group">
                <div class="bg-green bd-blue p-10-20 w-30 me-20" for="name">Họ và
                    tên</div>
                <input class=" fl-1" type="text" id="name" name="name" required>
            </div>

            <div class="form-group">
                <div class="bg-green  bd-blue p-10-20 w-30 me-20" for="gender">
                    Giới tính</div>
                <div id="gender" name="gender" class="w-170 ">
                    <input type="radio" id="male" name="gender" value="Nam" required> Nam
                    <input type="radio" id="female" name="gender" value="Nữ" required> Nữ
                </div>
            </div>

            <div class="form-group">
                <label for="birthday" id="birthday" name="birthday">Ngày Sinh:</label>
                <select name="year" id="year" required>
                    <?php
                    $currentYear = date("Y");
                    for ($year = $currentYear - 20; $year >= $currentYear - 80; $year--) {
                        echo '<option value="' . $year . '">' . $year . '</option>';
                    }
                    ?>
                </select>
                <select name="month" id="month" required>
                    <?php
                    for ($i = 1; $i <= 12; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }
                    ?>
                </select>
                <select name="day" id="day" required>
                    <?php
                    for ($i = 1; $i <= 31; $i++) {
                        echo '<option value="' . $i . '">' . $i . '</option>';
                    }
                    ?>
                </select><br><br>
            </div>


            <div class="form-group">
                <label for="address" id="address" name="address">Thành Phố:</label>
                <select name="city" id="city" onchange="updateDistricts();" required>
                    <option value=""></option>
                    <option value="Hà Nội">Hà Nội</option>
                    <option value="TP.HCM">TP.HCM</option>
                </select><br><br>

                <label for="district">Quận:</label>
                <select name="district" id="district">
                    <option value=""></option>
                </select><br><br>


            </div>

            <div class="form-group">
                <div class="bg-green bd-blue p-10-20 w-30 " for="infor">Thông tin khác
                </div>
                <input class="" name="infor" id="infor"></input>
            </div>

            <div class="button-container" id="registerButton">
                <button type="submit">Đăng ký</button>
            </div>
        </form>
    </div>
</body>

</html>
